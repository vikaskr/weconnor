<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <base href="" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="Connor, Connor Group, Asian Buying Agent, Sourcing Services, Global Sourcing, Supply Chain Management, Social Compliance, Technical Evaluation, Quality Management, Order Tracking, Furniture, ISO9002, World Class Service, Client Centric." />
  <meta name="title" content="What We Are" />
  <meta name="author" content="Basil Wong" />
  <meta name="description" content="The Connor Group is a world-wide merchandise-sourcing organization, managing the global sourcing requirements of over 60 companies." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Home | International Company in merchandise sourcing</title>
  <link href="https://www.weconnor.com/templates/Weconnor_Inner/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/css/style.css" type="text/css" />
  <script type="text/javascript" src="media/system/js/mootools.js"></script>
  <script type="text/javascript" src="media/system/js/caption.js"></script>
  <script type="text/javascript" src="media/system/js/swf.js"></script>
  <script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
  <script type="text/javascript" src="/js/jquery.innerfade.js"></script>

<!-- <link type="text/css" rel="stylesheet" href="/css/style.css" /> -->

<link type="text/css" rel="stylesheet" href="/css/layout.css" />
<link type="text/css" rel="stylesheet" href="/css/menu.css" />
<link type="text/css" rel="stylesheet" href="/css/joomla.css" />
<link type="text/css" rel="stylesheet" href="/css/template.css" />
<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie8.css" />
<![endif]-->
<!--[if IE 7]>
<link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie7.css" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="/js/jquery.splashdiv.js"></script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify.2.1.0.js"></script>
<script type="text/javascript" src="/js/infinityforms.js"></script>

<script type="text/javascript">
    var jQuery14 = jQuery.noConflict();
    
    

    // Custom javascripts
    //$("#inf_menu_main li.lv_1.item_33 a span").css("border","13px solid red");
    jQuery14(document).ready(function() {
    //    jQuery14(".mainmenu #inf_menu_main li.lv_1.item_33 a").hover(function(){
    //        jQuery("#div_hidden_1").show();
    //    }, function(){
    //        jQuery("#div_hidden_1").hide();
    //    });
      // alert("ok");   

    
//      jQuery14("#dlg_splash").splashDiv({
//            hoverHold:true
//          , cookieCheck:true
//          , height: 520
//          , show: {effect: 'blind', duration: 1000}
//          , hide: {effect: 'blind', duration: 500}
//          , onShow: function() {
//              jQuery14("#dlg_splash_btn_close").unbind('click');
//              jQuery14("#dlg_splash_btn_close").bind('click',function(){
//                  jQuery14("#dlg_splash").dialog("close");
//              });
//            }
//          //, onHide: function() {alert('hide');}
//      });
       
     

    });

    function hideContent(d) {
        document.getElementById(d).style.display = "none";
    }

    function showContent(d) {
        document.getElementById(d).style.display = "block";
    }

</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23262756-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>
</head>
<body id="home">
<div class="layout">
<div class="wrapper">
<div class="bg">
    <div class="header">
        <!-- logo -->
        <div class="logo">
            		<div class="moduletable">
					<p><img src="/images/stories/connor_logo.png" height="54" width="250" /></p>		</div>
	
        </div>
        <!-- logo -->
        
        <!-- top_slogan -->
            <div class="top_slogan">
              <div class="ly1">
                    <div class="moduletable">
          <img alt="fb-icon" height="20" width="20" src="/images/stories/fb-icon.jpg" /></a> 
<img src="/images/In-2C-14px.png" title="LinkedIn" height="20" width="20" style="cursor:pointer" onclick="window.open('https://www.linkedin.com/company-beta/124218/?pathWildcard=124218');" /> <p style="font-size: 15px;">We will represent one interest and only one interest, that of our Principal,<br />
                    </div>
  
                </div>
            </div>
        <!-- top_slogan -->        
        <div class="clear-both"></div>
        <!-- mainmenu -->
        <div class="mainmenu">
            		<div class="moduletable">
					
<div class="inf_menu_h" id="inf_menu_main">
		<ul><li class="lv_1 item_5 first"><a  target="_self" href="/index.php"><span>Home</span></a></li><li class="lv_1 item_6 parent"><a  target="_self" href="/company.php"><span>Company Information</span></a><ul><li class="lv_2 item_34"><a  target="_self" href="/vision.php"><span>Vision Statement</span></a></li><li class="lv_2 item_20 last"><a  target="_self" href="/faq.php"><span>FAQ</span></a></li></ul></li><li class="lv_1 item_7 parent"><a  target="_self" href="/services.php"><span>Services</span></a><ul><li class="lv_2 item_24 first"><a  target="_self" href="/products.php"><span>Products Sourced</span></a></li><li class="lv_2 item_28"><a  target="_self" href="/quality.php"><span>Quality Assurance</span></a></li><li class="lv_2 item_29 last"><a  target="_self" href="/mis.php"><span>MIS</span></a></li></ul></li><li class="lv_1 item_35 parent"><a  target="_self" href="/newsroom.php"><span>Newsroom</span></a><ul><li class="lv_2 item_36 first"><a  target="_self" href="/announcements.php"><span>Announcements</span></a></li><li class="lv_2 item_37"><a  target="_blank" href="#"><span>Vsource Facebook</span></a></li><li class="lv_2 item_39"><a  target="_self" href="/media.php"><span>Media</span></a></li><li class="lv_2 item_45 last"><a  target="_blank" href="#"><span>LinkedIn</span></a></li></ul></li><li class="lv_1 item_9 parent current"><a  target="_self" href="/career.php"><span>Careers</span></a><ul><li class="lv_2 item_41 first"><a  target="_self" href="career-home.php"><span>Home</span></a></li><li class="lv_2 item_42"><a  target="_self" href="/working.php"><span>Working in Vsource</span></a></li><li class="lv_2 item_43 last"><a  target="_self" href="/careers.php"><span>Career Opportunities</span></a></li></ul></li><li class="lv_1 item_8 last"><a  target="_self" href="/contact.php"><span>Contact Us</span></a></li></ul></div>
		</div>
	
        </div>
        <!-- mainmenu -->
        <div class="clear-both"></div>
    </div>
	
	<div id="maincontainer">
        <div class="main">
             <div class="banner">
            <div class="moduletable">
                    
                <div class="infbanners" id="infbanners-1" style="height:190px;">
                    <div class="banners" id="infbanners-1-frame">
                        <div class="banner" id="tab1">
                            <a href="home#">
                                <img style="width: 952px; height: 170px;" alt="Desert" src="/images/13.png" title="slideshow5">
                            </a>
                        </div>              
                    </div>  
                </div>

    <div class="container" style="padding-top: 0px; min-height: 30px!important;  border-bottom: 1px solid #d0d0d0; padding: 5px 0 5px 0">
        <ul><li>Home <img src="/images/download.png" height="10px"> Career <img src="/images/download.png" height="10px"> <span style="color: #8a6700;">Working in Vsource</span></li></ul>
    </div>

<div class="container" style="padding-top: 20px;">
            	
				
<div id="page">

<h1 class="contentheading">
	Working in Vsource</h1>

<p>Our success depends on the motivation, competence and retention of our people, hence, we have developed a comprehensive framework to recognize talent and develop individuals to their fullest potentials.</p><br>

<p>We recognize that you may aspire to career growth as either an Individual Contributor or as a Leader. Regardless of the career stage you find yourself in, we engage and support you by means of evaluation, discussion, planning and development activities that are appropriate to your aspirations and current level of competence, performance and potential.</p><br>

<p>We are serious about our associates' development. We offer diverse combinations of on-the-job coaching and mentoring, training programs, opportunities to travel, as well as attachments with Connor businesses or projects abroad. Such initiatives have resulted in a high performing, innovative and motivated workforce.</p><br>

<p>If you would like a career in Connor please <span style="color: #0d1f84; font-size: 15px;">APPLY NOW.</span></p><br>

<p>Personal data provided by applicants is handled by our professional global human resources team. All information will be treated in the strictest confidence. </p><br>

</div>

            	<div class="clear-both"></div>
            </div>
            	
        </div>
	</div>	
    
</div>
    
<div class="footer">

            <div class="copyright">		<div class="moduletable">
					<p style="display:inline">Copyright © 2017 William E. Connor &amp; Associates Ltd. All Rights Reserved. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Join Us: &nbsp;<a target="_blank" href="https://www.facebook.com/ConnorGroup"><img alt="fb-icon" height="17" width="16" src="/images/stories/fb-icon.jpg" /></a> facebook</p>&nbsp;&nbsp;&nbsp;&nbsp;
<!-- script src="//platform.linkedin.com/in.js" type="text/javascript">
  lang: en_US
</script>
<script type="IN/FollowCompany" data-id="124218" data-counter="none"></script //-->
<img src="/images/In-2C-14px.png" title="LinkedIn" style="cursor:pointer" onclick="window.open('https://www.linkedin.com/company-beta/124218/?pathWildcard=124218');" />	LinkedIn	</div>
	</div>
    </div>
</div>
</div>

<div id="div_hidden_1" style="display:none; background-color:#B5F484; position:absolute; top:50px; left:440px; color:black; font-style:bold;">
     Coming Soon.....
</div>
<div id="dlg_splash" style="display:none;color:black;">
    <img src="/images/splash/splash-header.png" width="300"></img>
    <div style="clear:both;text-align:left;line-height:130%;font-size:20pt;font-family:'Book Antiqua';">
        <br/>
        Connor named as one of the World's Most Ethical Companies.
        <br/>
        <br/>
    </div>
    <div style="clear:both;text-align:left;line-height:120%;font-size:14pt;font-family:'Book Antiqua';">
        Connor was honored to be recognized by Ethisphere and included in their list of World’s Most Ethical Companies for the sixth consecutive year in 2017. We are the only sourcing firm in the world to be included.
        <br/>
      <br/>
    </div>
    <a target="blank" href="http://www.ethisphere.com"><img src="/images/WME_2017_Logo.jpg" width="180" align="right" style="border-style: none"></img></a>
    <div style="clear:both;text-align:right;">
        <br/>
        <button id="dlg_splash_btn_close">Close</button>
    </div>
</div>

</body>
</html>