/**
 * Infinity Form Object
 *
 * Example:
 * $('#form').infinityform({
 *     rand      : 1000,                                                    // <required> random number of the form
 *     uri       : '',                                                      // <required> uri of the site
 *     set_title : true,                                                    // <optional> set blank on focus & set caption on blur to the fields if here is `true`
 *     msg       : {                                                        // <optional> messages
 *         error     : 'Please fill in correct %s.',                        // <optional> pop up message for error data, %s will be replaced with the value of caption
 *         uploading : 'The attachment(s) are uploading, please wait.'      // <optional> pop up message for submitting when uploading attachment(s)
 *     },
 *     data_type : {                                                        // <optional> form checking rules
 *         required : { reg : /\d+/, msg : 'Please fill in correct %s.' },  // <optional> here %s will be replaced with the value of caption
 *         number   : /\d+/                                                 // <optional> the function is goging to use `Please fill in correct %s.` as default error message if not set
 *     },
 *     att_opts  : {                                                        // <optional> settings of attachments
 *         fileDesc       : 'Allowed file type: jpg, jpegm png, gif',       // <optional> description of file types
 *         fileExt        : '*.jpg;*.jpeg;*.png;*.gif;',                    // <optional> file types limit
 *         sizeLimit      : 1024 * 1024 * 10,                               // <optional> file size limit in bytes
 *         simUploadLimit : 1                                               // <optional> files limit
 *     }
 * });
 *
 */
(function($)
{
	$.fn.infinityform = function(options)
	{
		var opts = $.extend({
			rand      : 1000,
			uri       : '',
			set_title : false,
			msg       : {
				error     : 'Please fill in correct %s.',
				uploading : 'The attachment(s) are uploading, please wait.'
			},
			data_type : {
				required   : { reg : /\S+/, msg : 'Please fill in %s.', msg_multi : 'Please select an option of $s.' },
				email      : /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/,
				validation : /^\w{5}$/
			},
			att_opts  : {
				fileDesc       : 'Allowed file type: jpg, jpegm png, gif, txt, zip, rar, pdf, doc, docx',
				fileExt        : '*.jpg;*.jpeg;*.png;*.gif;*.txt;*.zip;*.rar;*.pdf;*.doc;*.docx',
				sizeLimit      : 1024 * 1024 * 2,
				simUploadLimit : 3
			}
		}, options);
		
		var form = this;
		var validation_img = form.find('.form_infinityform_validation_img');
		var is_uploading = false;
		
		// refresh validation img on clicked
		validation_img.css({ 'cursor':'pointer' }).click(function() {
			$(this).attr('src', opts.uri + '/index.php?option=com_infinityforms&task=validation&rand=' + opts.rand + '&ra=' + Math.round(Math.random() * 10000));
		});
		
		// rebuild some form elements
		form.find('input:text, input:password, select, textarea').each(function(i, el) {
			el = $(el);
			var id = 'infinityform_' + opts.rand + '_' + el.attr('name').toLowerCase().replace(/\W/, '');
			el.attr('id', id);
			if (el.parent('label'))
				el.parent('label').attr('for', id);
		});
		
		// add captions & onfocus|onblur events
		if (opts.set_title) {
			form.find('input:text, textarea').each(function(i, el) {
				el = $(el);
				var caption = el.attr('caption');
				el.attr('value', caption);
				el.focus(function() {
					if (el.attr('value') == caption)
						el.attr('value', '');
				}).blur(function() {
					if (el.attr('value') == '')
						el.attr('value', caption);
				});
			});
		}
		
		// add attachments
		$("#btn_infinityform_attachments_" + opts.rand).uploadify
		({
			// fixed settings
			'uploader'    : opts.uri + '/components/com_infinityforms/js/uploadify/uploadify.swf',
			'script'      : opts.uri + '/index.php%3Foption%3Dcom_infinityforms%26task%3DuploadAttachment%26rand%3D' + opts.rand, // url after url-encode
			'cancelImg'   : opts.uri + '/components/com_infinityforms/images/cancel.png',
			'queueID'     : 'list_infinityform_attachments_' + opts.rand,
			'auto'        : true,
			'multi'       : true,
			
			// events
			'onProgress' : function(evt, queueID, fileObj, data) {
				is_uploading = true;
			},
			'onComplete' : function(evt, queueID, fileObj, response, data) {
				var att_files = $('#infinityform_att_files_' + opts.rand);
				att_files.attr('value', att_files.attr('value') + ',' + fileObj.name);
			},
			'onAllComplete' : function(evt, data) {
				is_uploading = false;
			},
			
			// optional settings
			'fileDesc'       : opts.att_opts.fileDesc,
			'fileExt'        : opts.att_opts.fileExt,
			'sizeLimit'      : opts.att_opts.sizeLimit,
			'simUploadLimit' : opts.att_opts.simUploadLimit
		});
		
		// do NOT do the processing on ajaxSetup success event for the probable return of incorrect form
		form.ajaxSuccess(function(evt, request, settings)
		{
			// get returns
			try {
				eval('(' + request.responseText + ')');
			}
			catch(e) {
				alert(request.responseText);
				return;
			}
			var result = eval('(' + request.responseText + ')');
			
			// check if is the right form
			if (result.rand == opts.rand)
			{
				// alert message
				alert(result.msg);
				
				// do some processing if successed
				if (result.statu) {
					if (result.url)
						window.location.href = result.url;
					else
						resetForm(form, opts);
				}
				
				// reflesh the validation image after submit
				validation_img.attr('src', opts.uri + '/index.php?option=com_infinityforms&task=validation&rand=' + opts.rand + '&ra=' + Math.round(Math.random() * 10000));
				
				// enable submitting
				form.find('input:submit').removeClass('disabled').attr('disabled', '');
			}
		});
		
		// communication with PHP function
		$.ajaxSetup({
			url  : this.attr('action'),
			type : 'post'
		});
		
		form.submit(function()
		{
			// check form
			if (!checkForm(form, opts))
				return false;
			
			// check if is uploading
			if (is_uploading) {
				alert(opts.msg.uploading);
				return false;
			}
			
			// disable submitting
			form.find('input:submit').addClass('disabled').attr('disabled', 'disabled');
			
			// send data
			$.ajax({ data : form.serialize() });
		});
		
		return this;
	};
	
	function checkForm(form, opts)
	{
		var auth = true;
		var error = '';
		var first_err_item;
		
		form.find('input:text, input:password, input:checkbox, input:radio, select, textarea').each(function(i, el)
		{
			el = $(el);
			var data_types = opts.data_type;
			for (var t in data_types) {
				if (el.hasClass(t)) {
					// check boxes & radio required checking
					if (t == 'required' && $.inArray(el.attr('type'), ['checkbox', 'radio']) > -1) {
						var msg = data_types.required.msg_multi;
						var checked = false;
						form.find('input[name="' + el.attr('name') + '"]').each(function(i, ele) {
							if (ele.checked)
								checked = true;
						});
						if (!checked) {
							var my_msg = msg.replace('%s', el.attr('caption'));
							if (error.indexOf(my_msg) > -1)
								error += my_msg + '\n';
							auth = false;
						}
					}
					// other checkings by rules
					else {
						var reg = data_types[t].reg ? data_types[t].reg : data_types[t];
						var msg = data_types[t].msg ? data_types[t].msg : opts.msg.error;
						
						// error processing
						if (!reg.test(el.val())) {
							if (el.parent('label') && !el.parent('label').hasClass('error'))
								el.parent('label').addClass('error');
							error += msg.replace('%s', el.attr('caption')) + '\n';
							auth = false;
							if (!first_err_item)
								first_err_item = el;
							break;
						}
						else {
							if (el.parent('label'))
								el.parent('label').removeClass('error');
						}
					}
				}
			}
		});
		
		if (!auth) {
			alert(error);
			first_err_item.focus();
			return false;
		}
		
		return true;
	}
	
	function resetForm(form, opts)
	{
		form.get(0).reset();
		if (opts.set_title) {
			form.find('input:text, textarea').each(function(i, el) {
				el = $(el);
				el.attr('value', el.attr('caption'));
			});
		}
		if ($('#infinityform_att_files_' + opts.rand))
			$('#infinityform_att_files_' + opts.rand).attr('value', '');
	}
	
})(jQuery);