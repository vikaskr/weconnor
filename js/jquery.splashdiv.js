//stephen, 2012-03-14
//show splash div


//splashDiv plugin
jQuery.fn.extend({ 
    //need to use jquery ui dialog
    splashDiv: function(ao_options) {
        var lo_defaults = {
            delay: 10
          , checkHoverDelay: 5
          , height: 500
          , width: 800
          , show: 'blind'
          , hide: 'blind'
          , cookieCheck: true
          , cookieName: 'splash_div'
          , cookieExpires: 0
          , onShow: function() {}
          , onHide: function() {}
          , hoverHold: true
          , hoverClass: 'dlg_splash_onhover'
        };
        var lo_settings = jQuery.extend(lo_defaults, ao_options);
        lo_settings.delayMillisec = lo_settings.delay * 1000;
        lo_settings.checkHoverDelayMillisec = lo_settings.checkHoverDelay * 1000;

        function createCookie(name,value,days) {
          if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
          }
          else { 
            var expires = "";
          }
          document.cookie = name+"="+value+expires+"; path=/";
        }

        function readCookie(name) {
          var nameEQ = name + "=";
          var ca = document.cookie.split(';');
          for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)===' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
          }
          return null;
        }
        function closeSplashDialog(ao_next, ao_dlg_element) {
            if (lo_settings.hoverHold) {
                if (jQuery('.'+lo_settings.hoverClass).length > 0 ) {
                    jQuery(ao_dlg_element).delay(lo_settings.checkHoverDelayMillisec).queue(function(){
                        closeSplashDialog(ao_next, ao_dlg_element); 
                    });
                    ao_next();
                    return;
                }        
            }
            lo_settings.onHide(ao_dlg_element);
            jQuery(ao_dlg_element).dialog( "close" );
        }
        
        return this.each(function() {
            var lo_element = jQuery(this);
            if (lo_settings.cookieCheck) {
                if(readCookie(lo_settings.cookieName) == "dontshow"){
                    return;
                }
            }
            lo_settings.onShow(this);
            jQuery(lo_element).dialog({
                  modal:true
                , height:lo_settings.height
                , width :lo_settings.width
                , show  :lo_settings.show
                , hide  :lo_settings.hide
                , resizable: false
                , close:function() {
                      if (lo_settings.cookieCheck) {
                          createCookie(lo_settings.cookieName,'dontshow', lo_settings.cookieExpires); 
                      }
                      jQuery(lo_element).clearQueue();
                  }
                , open:function() {
                      var lo_dlg_parent = jQuery(lo_element).parent();
                      lo_dlg_parent.addClass("dlg_splash_parent");
                      lo_dlg_parent.css("border", "2px grey solid");
                      var lo_header = jQuery(lo_element).parent().children(".ui-widget-header");
                      lo_header.css("border","0px");
                      lo_header.css("background","white");
                      lo_header.css("color","white");
                      
                      var lo_close_button_div = jQuery(".ui-dialog-titlebar-close", lo_header);
                      //lo_close_button_div.unbind('mouseenter').unbind('mouseleave');
                      //lo_close_button_div.addClass("ui-state-hover");
                      lo_close_button_div.hide();
                      
                      if (lo_settings.hoverHold) {
                          jQuery(lo_element).hover(
                            function() {
                                jQuery(lo_element).addClass(lo_settings.hoverClass);
                            }
                            ,
                            function() {
                                jQuery(lo_element).removeClass(lo_settings.hoverClass);
                            }
                          );
                      }
                      jQuery(lo_element).delay(lo_settings.delayMillisec).queue(function(ao_next){
                          closeSplashDialog(ao_next, lo_element); 
                      })
                  }
            });

        });    
    }
});