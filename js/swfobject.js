/* =========================================================

// jquery.innerfade.js

// Datum: 2008-02-14
// Firma: Medienfreunde Hofmann & Baldes GbR
// Author: Torsten Baldes
// Mail: t.baldes@medienfreunde.com
// Web: http://medienfreunde.com

// based on the work of Matt Oakes http://portfolio.gizone.co.uk/applications/slideshow/
// and Ralf S. Engelschall http://trainofthoughts.org/

 *
 *  <ul id="news"> 
 *      <li>content 1</li>
 *      <li>content 2</li>
 *      <li>content 3</li>
 *  </ul>
 *  
 *  $('#news').innerfade({ 
 *    animationtype: Type of animation 'fade' or 'slide' (Default: 'fade'), 
 *    speed: Fading-/Sliding-Speed in milliseconds or keywords (slow, normal or fast) (Default: 'normal'), 
 *    timeout: Time between the fades in milliseconds (Default: '2000'), 
 *    type: Type of slideshow: 'sequence', 'random' or 'random_start' (Default: 'sequence'), 
 *      containerheight: Height of the containing element in any css-height-value (Default: 'auto'),
 *    runningclass: CSS-Class which the container getâ€™s applied (Default: 'innerfade'),
 *    children: optional children selector (Default: null)
 *  }); 
 *

// ========================================================= */


(function(jQuery) {
    var innerfadeTimeoutId = null;
    jQuery.fn.innerfade = function(options) {
        return this.each(function() {   
            jQuery.innerfade(this, options);
        });
    };

    jQuery.innerfade = function(container, options) {
        var settings = {
            'animationtype':    'fade',
            'speed':            'normal',
            'timeout':          2000,
            'containerheight':  'auto',
            'runningclass':     'innerfade',
            'children':         null,
            'bar':  null
        };
        if (options)
            jQuery.extend(settings, options);
        if (settings.children === null)
            var elements = jQuery(container).children();
        else
            var elements = jQuery(container).children(settings.children);
        
        if (elements.length > 1) {
            jQuery(container).css('position', 'relative').css('height', settings.containerheight).addClass(settings.runningclass);
            for (var i = 0; i < elements.length; i++) {
                jQuery(elements[i]).css('z-index', String(elements.length-i)).css('position', 'absolute').hide();
            };

            innerfadeTimeoutId = setTimeout(function() {
                jQuery.innerfade.next(elements, settings, 1, 0);
            }, settings.timeout);
            jQuery(elements[0]).show();
            if(settings.bar != null){
                jQuery(settings.bar + '  li.tab1 a').addClass('selected');
            }

            if(settings.bar != null){
                var bars = jQuery(settings.bar + '  li');
                bars.each(function(i){
                    var obj = this;
                    jQuery(this).bind('click', function() {
                        var currentTab =  parseInt(obj.className.substring(3)) -1;
                        var lastTab = parseInt(jQuery(settings.bar + '  li a.selected').get(0).parentNode.className.substring(3)) -1;
                        
                        if(currentTab != lastTab){
                            if(innerfadeTimeoutId != null) clearTimeout(innerfadeTimeoutId);
                            innerfadeTimeoutId = setTimeout(function(){
                                jQuery.innerfade.next(elements, settings, currentTab, lastTab);
                            }, 0);
                        }
                    });

                });
            }
        }
    };

    jQuery.innerfade.next = function(elements, settings, current, last) {
        if (settings.animationtype == 'slide') {
            jQuery(elements[last]).slideUp(settings.speed);
            jQuery(elements[current]).slideDown(settings.speed);
        } else if (settings.animationtype == 'fade') {
            jQuery(elements[last]).fadeOut(settings.speed);
            jQuery(elements[current]).fadeIn(settings.speed, function() {
                removeFilter(jQuery(this)[0]);
            });
        } else{
            alert('Innerfade-animationtype must either be \'slide\' or \'fade\'');
        }

        if ((current+1) <= elements.length) {
            barCurrent = current+1;
            barLast = last>0 ? last : (barCurrent == 1 ? elements.length : current);
        }else{
            barCurrent = 1;
            barLast = last>0 ? last : elements.length;
        }
        
        if(settings.bar != null){
            jQuery(settings.bar + '  li a.selected').removeClass('selected');
            jQuery(settings.bar + '  li.tab'+ barCurrent + ' a' ).addClass('selected');
        }

        if ((current + 1) < elements.length) {
            current = current + 1;
            last = current - 1;
        } else {
            current = 0;
            last = elements.length - 1;
        }

        if(innerfadeTimeoutId != null) clearTimeout(innerfadeTimeoutId);
        innerfadeTimeoutId = setTimeout((function() {
            jQuery.innerfade.next(elements, settings, current, last);
        }), settings.timeout);
    };

})(jQuery);

// **** remove Opacity-Filter in ie ****
function removeFilter(element) {
    if(element.style.removeAttribute){
        element.style.removeAttribute('filter');
    }
}