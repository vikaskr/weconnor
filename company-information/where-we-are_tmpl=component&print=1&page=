<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="noindex, nofollow" />
  <meta name="keywords" content="attention to detail, global field presence." />
  <meta name="title" content="The Connor Difference" />
  <meta name="author" content="webmaster" />
  <meta name="description" content="The Connor Group is a world-wide merchandise-sourcing organization, managing the global sourcing requirements of over 60 companies." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Where we are | Field presence sourcing merchandise in factories</title>
  <link href="https://www.weconnor.com/templates/Weconnor_Inner/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <script type="text/javascript" src="../media/system/js/mootools.js"></script>
  <script type="text/javascript" src="../media/system/js/caption.js"></script>

	<link type="text/css" rel="stylesheet" href="https://www.weconnor.com/templates/system/css/system.css" />
    <link type="text/css" rel="stylesheet" href="https://www.weconnor.com/templates/system/css/general.css" />
    <link type="text/css" rel="stylesheet" href="https://www.weconnor.com/templates/Weconnor_Home/css/template_css.css"/>
    <!--[if IE 8]>
    <link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie8.css" />
    <![endif]-->
    <!--[if IE 7]>
    <link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie7.css" />
    <![endif]-->

</head>

<body class="contentpane">
    <div class="printbutton">
        <a href="javascript:void(0)" onclick="javascript:window.print();"></a>
    </div>
    <div class="print-layout">
        <div class="header">
            <!-- logo -->
            <div class="logo">
                		<div class="moduletable">
					<p><img src="../images/stories/connor_logo.png" height="54" width="250" /></p>		</div>
	
            </div>
            <!-- logo -->
            <div class="clear-both"></div>
        </div>

        <div class="bg">
            <div id="maincontainer">
                <div class="main">
                    
                    
<div id="page">


<h1 class="contentheading">
	The Connor Difference</h1>









<p><strong>Field Presence </strong>Technology cannot replace the hands-on management of thousands of details in the field and around the world. Managing the flow of goods from factory floor to distribution center or selling floor is Connor's core competency. This cannot be accomplished electronically or by remote control. We manage a universe of detail and complexity with proven personnel on the ground in the countries, communities, and factories where our clients' interests lie. This is "field presence."</p>
<p>"Managing a business in Tianjin from Hong Kong is like running a factory in Guadalajara from Chicago.&nbsp; There is only one way to manage the business.&nbsp; To have dedicated people on the ground, in all places, all the time."</p>

<h2>Connor Global Network</h2>

<div style="position: relative; margin: auto; width: 892px;"><img height="509" width="892" src="../images/stories/map_03.png" />
<div style="z-index: 1; position: absolute; display: block; color: black; top: 233px; left: 625px;">&nbsp;<a href="where-we-are#corporate_office" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a></div>
<div class="showtip" id="corporate_office" style="z-index: 999; position: absolute; background-color: #ffffff; width: 285px; display: none; top: 237px; left: 654px;">
<div class="tip_title">Corporate Office</div>
The Harbourfront, 15/F, Office Tower II, 18-22 Tak Fung Street, Hung Hom, Kowloon, Hong Kong<br />
<div class="tip_title">William E Connor &amp; Associates Limited</div>
The Harbourfront, 6/F, Office Tower II, 18-22 Tak Fung Street, Hung Hom, Kowloon, Hong Kong<br />
<div class="tip_title">Omega Compliance Ltd.</div>
603 The Harbourfront II, 18-22 Tak Fung Street Hung Hom, Kowloon, Hong Kong</div>

<div style="z-index: 1; position: absolute; display: block; color: black; top: 224px; left: 589px;">&nbsp;<a href="where-we-are#bangladesh" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a></div>
<div class="showtip" id="bangladesh" style="z-index: 999; position: absolute; background-color: #ffffff; width: 320px; display: none; top: 227px; left: 620px;">
<div class="tip_title">Connor International Limited</div>
 Mika Cornerstone, 12th Floor, Plot no. 16 &amp; 17, Road No.- 12, Sector-6, Uttara, Dhaka, Bangladesh 
</div>

<div style="z-index: 1; position: absolute; display: block; color: black; top: 211px; left: 644px;"><a href="where-we-are#china" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="china" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 240px;">
<div class="tip_title">William E Connor (Shanghai) Co., Limited</div>
8F, United Power International Plaza, No. 1158, Jiangning Rd, Shanghai, China 200060</div>


<div style="z-index: 1; position: absolute; display: block; color: black; top: 225px; left: 635px;"><a href="where-we-are#shenzhen" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="shenzhen" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 645px;">
<div class="tip_title">William E Connor &amp; Associates Limited<br />Shenzhen Representative Office</div>
Unit D, 5/F, Block CD, Tian Xiang, Chegongmiao, Futian, Shenzhen, China, 518042</div>

<div style="position: absolute; display: block; color: black; top: 212px; left: 552px;"><a href="where-we-are#india" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="india" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 216px; left: 182px;">
<div class="tip_title">William E. Connor &amp; Associates (Sourcing) Private Limited</div>
Spazedge Commercial Tower, Tower B, 1st Floor, Sector – 47, Sohna Road, Gurgaon, Haryana, PIN – 122002, India</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 210px; left: 558px;"><a href="where-we-are#qac_mor" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_mor" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 640px;">
<div class="tip_title">Moradabad, India</div></div>

<div style="position: absolute; display: block; color: black; top: 326px; left: 640px;"><a href="where-we-are#indonesia" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="indonesia" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 334px; left: 275px;">
<div class="tip_title">William E Connor &amp; Associates Limited<br />Indonesia Representative Office</div>
Menara Dea II 6th Floor, Jl. Mega Kuningan Barat Kav.E.4.3. No.1-2, Jakarta Selatan 12950, Indonesia</div>

<div style="position: absolute; display: block; color: black; top: 266px; left: 617px;"><a href="where-we-are#vietnam" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="vietnam" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 270px; left: 249px;">
<div class="tip_title">William E Connor &amp; Associates Limited<br />Resident Representative Office in Vietnam</div>
Unit 9.2, 9th Floor, E.town 2 Building, 364 Cong Hoa Street, Tan Binh District, Ho Chi Minh City, Vietnam</div>

<div style="position: absolute; display: block; color: black; top: 196px; left: 267px;"><a href="where-we-are#usa" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="usa" style="z-index: 999; position: absolute; background-color: #ffffff; width: 280px; display: none; top: 200px; left: -25px;">
<div class="tip_title">WorldWindows, LLC</div>
15W 34th Street, 6th Floor, New York, NY 10001 </div>

<div style="position: absolute; display: block; color: black; top: 180px; left: 462px;"><a href="where-we-are#turkey" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="turkey" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 212px; left: 109px;">
<div class="tip_title">Connor International Limited<br />Istanbul Liaison Office</div>
Yasemen Sokak no:18 Fidan Apt.  D:1-2  pk:34464  Yenikoy  Sariyer  Istanbul Turkey</div>

<div style="position: absolute; display: block; color: black; top: 255px; left: 600px;">&nbsp;<a href="where-we-are#thailand" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a></div>
<div class="showtip" id="thailand" style="z-index: 999; position: absolute; background-color: #ffffff; width: 315px; display: none; top: 261px; left: 623px;">
<div class="tip_title">William E Connor (Thailand) Limited</div>
11th Floor, BUI Building, 177/1 Soi Anumarn –Rajchathon 1 Surawongse Road, Suriyawong, Bangrak, Bangkok 10500, Thailand</div>

<div style="position: absolute; display: block; color: black; top: 231px; left: 648px;"><a href="where-we-are#taiwan" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="taiwan" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 234px; left: 279px;">
<div class="tip_title">William E Connor (Taiwan) Limited</div>
CTCI Building, 22/F, No. 77, Sec. 2, Dunhua South Road, Taipei 106, Taiwan R.O.C.</div>

<div style="position: absolute; display: block; color: black; top: 272px; left: 565px;">&nbsp;<a href="where-we-are#sri_lanka" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a></div>
<div class="showtip" id="sri_lanka" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 276px; left: 590px;">
<div class="tip_title">William E Connor &amp; Associates Limited<br />Sri Lanka Liaison Office</div>
 No 41 2/1,

Dutugemunu Street,

Pamankada,

Dehiwala,

Sri Lanka</div>

<div style="position: absolute; display: block; color: black; top: 181px; left: 389px;"><a href="where-we-are#portugal" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="portugal" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 185px; left: 20px;">
<div class="tip_title">Connor International Limited (Portugal)</div>
Rua da Lionesa 446, Edificio C15, 4465-671 Leca do Balio, Portugal</div>

<div style="position: absolute; display: block; color: black; top: 256px; left: 650px;"><a href="where-we-are#philippines" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="philippines" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 256px; left: 280px;">
<div class="tip_title">Southgate Limited</div>
Unit 26A, 26th Floor, Petron Mega Plaza Building, 358 Sen. Gil Puyat Avenue, Makati City, Philippines 1200</div>

<div style="position: absolute; display: block; color: black; top: 195px; left: 663px;"><a href="where-we-are#korea" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="korea" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 201px; left: 294px;">
<div class="tip_title">Regis Gard Limited</div>
9F, Hyundai Intellex Bldg., 620, Eonju-ro, Gangnam-Gu,Seoul, 06101 Korea</div>

<div style="position: absolute; display: block; color: black; top: 170px; left: 425px;"><a href="where-we-are#italy" class="maphot_bg"><img src="https://www.weconnor.com/templates/Weconnor_Home/images/maphot.png" /></a>&nbsp;</div>
<div class="showtip" id="italy" style="z-index: 999; position: absolute; background-color: #ffffff; width: 354px; display: none; top: 201px; left: 294px;">
<div class="tip_title">Skelly &amp; Bonini Srl</div>
Via Tornabuoni, 1/A, Florence 50123 - Italy</div>


<div style="z-index: 0; position: absolute; display: block; color: black; top: 199px; left: 642px;"><a href="where-we-are#qac_qd" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_qd" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 240px;">
<div class="tip_title">Qingdao, China</div>
<!-- The operation of this QA office is controlled by<br />William E Connor (Shanghai) Co., Limited<br />8F, United Power International Plaza, No. 1158, Jiangning Rd, Shanghai, China 200041 //--></div>


<div style="z-index: 0; position: absolute; display: block; color: black; top: 230px; left: 625px;"><a href="where-we-are#qac_dg" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_dg" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 640px;">
<div class="tip_title">Dongguan, China</div>
<!-- The operation of this QA office is controlled by<br />William E Connor &amp; Associates Limited<br />The Harbourfront, 15/F, Office Tower II, 18-22 Tak Fung Street, Hung Hom, Kowloon, Hong Kong //--></div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 270px; left: 547px;"><a href="where-we-are#qac_al" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_al" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 570px;">
<div class="tip_title">Coimbatore, India</div>
<!-- The operation of this QA office is controlled by<br />William E Connor &amp; Associates Limited<br />India Liaison Office<br />13th Floor Signature Towers, South City, Gurgaon, Haryana, 122001, India //--></div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 267px; left: 551px;"><a href="where-we-are#qac_tiru" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_tiru" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 570px;">
<div class="tip_title">Tirupur, India</div>
</div>


<div style="z-index: 0; position: absolute; display: block; color: black; top: 270px; left: 556px;"><a href="where-we-are#qac_ba" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_ba" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 570px;">
<div class="tip_title">Bangalore, India</div>
<!-- The operation of this QA office is controlled by<br />William E Connor &amp; Associates Limited<br />India Liaison Office<br />13th Floor Signature Towers, South City, Gurgaon, Haryana, 122001, India //--></div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 207px; left: 478px;"><a href="where-we-are#qac_jo" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_jo" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 500px;">
<div class="tip_title">Ar Ramtha Irbid, Jordan</div>
<!-- The operation of this QA office is controlled by<br />William E Connor &amp; Associates Limited<br />India Liaison Office<br />13th Floor Signature Towers, South City, Gurgaon, Haryana, 122001, India //--></div>




<div style="z-index: 0; position: absolute; display: block; color: black; top: 265px; left: 610px;"><a href="where-we-are#qac_ca" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_ca" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 640px;">
<div class="tip_title">Phnom Penh, Cambodia</div>
<!-- The operation of this QA office is controlled by<br />William E Connor &amp; Associates Limited<br />Resident Representative Office in Vietnam<br />Unit 9.2, 9th Floor, E.town 2 Building, 364 Cong Hoa Street, Tan Binh District, Ho Chi Minh City, Vietnam //--></div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 340px; left: 650px;"><a href="where-we-are#qac_su" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_su" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Yogyakarta, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 340px; left: 660px;"><a href="where-we-are#qac_yo" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_yo" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Surabaya, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 345px; left: 665px;"><a href="where-we-are#qac_bali" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_bali" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Bali, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 335px; left: 645px;"><a href="where-we-are#qac_cir" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_cir" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Cirebon, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 345px; left: 660px;"><a href="where-we-are#qac_malang" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_malang" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Malang, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 335px; left: 650px;"><a href="where-we-are#qac_semu" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_semu" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Semarang, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 340px; left: 655px;"><a href="where-we-are#qac_solo" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_solo" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Solo, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 328px; left: 645px;"><a href="where-we-are#qac_subang" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_subang" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Subang, Indonesia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 290px; left: 605px;"><a href="where-we-are#qac_kua" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_kua" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 270px; left: 640px;">
<div class="tip_title">Kuala Lumpur, Malaysia</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 290px; left: 245px;"><a href="where-we-are#qac_mana" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_mana" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 240px;">
<div class="tip_title">Managua, Nicaragua</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 230px; left: 540px;"><a href="where-we-are#qac_kar" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_kar" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 640px;">
<div class="tip_title">Karachi, Pakistan</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 280px; left: 655px;"><a href="where-we-are#qac_cebu" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_cebu" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 640px;">
<div class="tip_title">Cebu, Philippines</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 255px; left: 649px;"><a href="where-we-are#qac_pam" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_pam" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 216px; left: 640px;">
<div class="tip_title">Pampanga, Philippines</div>
</div>


<div style="z-index: 0; position: absolute; display: block; color: black; top: 355px; left: 490px;"><a href="where-we-are#qac_mg" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_mg" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 356px; left: 570px;">
<div class="tip_title">Antananarivo, Madagascar</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 246px; left: 648px;"><a href="where-we-are#qac_tn" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_tn" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 356px; left: 570px;">
<div class="tip_title">Tainan, Taiwan</div>
<!-- The operation of this QA office is controlled by<br />William E Connor (Taiwan) Limited<br /> CTCI Building, 22/F., No.77, Dunhua South Road, Sec. 2, Taipei, Taiwan, R.O.C. //--></div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 195px; left: 462px;"><a href="where-we-are#qac_de" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_de" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 256px; left: 370px;">
<div class="tip_title">Denizli, Turkey</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 248px; left: 610px;"><a href="where-we-are#qac_ha" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_ha" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 356px; left: 570px;">
<div class="tip_title">Hanoi, Vietnam</div>
<!-- The operation of this QA office is controlled by<br />William E Connor &amp; Associates Limited<br />Resident Representative Office in Vietnam<br /> Unit 9.2, 9th Floor, E.town 2 Building, 364 Cong Hoa Street, Tan Binh District, Ho Chi Minh City, Vietnam //--></div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 248px; left: 602px;"><a href="where-we-are#qac_cm" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_cm" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 350px; left: 580px;">
<div class="tip_title">Chiang Mai, Thailand</div>
</div>

<div style="z-index: 0; position: absolute; display: block; color: black; top: 255px; left: 595px;"><a href="where-we-are#qac_yg" class="maphot_bg"><img src="../images/stories/orange-spot1.png" /></a></div>
<div class="showtip" id="qac_yg" style="z-index: 999; position: absolute; background-color: #ffffff; width: 390px; display: none; top: 366px; left: 565px;">
<div class="tip_title">Yangon, Myanmar</div>
</div>


<div></div>
</div>
<table>
<tr>
<td>
</td>
<td><img src="../images/redball.png" width="20" />&nbsp;&nbsp;<strong>Full Services Locations</strong>
</td>
<td>
</td>
<td>
</td>
<td><img src="../images/dot_orange.png" width="20" />&nbsp;&nbsp;<strong>QA / Service Locations</strong>
</td>
</tr>

<tr style="display:none">
<td valign="top">
<br/>
<img src="../images/redball.png" width="20" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>Hong Kong · Bangkok · Colombo · Dhaka · Florence · Gurgaon · Ho Chi Minh · Istanbul · 
Jakarta · Manila · New York · Porto · Seoul · Shanghai · Shenzhen · Taipei 
</td>
<td>
</td>
<td valign="top">
<br/>
<img src="../images/dot_orange.png" width="20" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>Phnom Penh · Qingdao · Dongguan · Bangalore · Coimbatore · Moradabad · Tirupur · Bali · Cirebon · Malang · Semarang · Solo · Subang · Surabaya · Yogyakarta · Ar Ramtha Irbid · Antananarivo · Kuala Lumpur · Managua · Karachi · Cebu · Pampanga · Tainan · Chiangmai · Denizli · Hanoi
</td>
</tr>

</table>

</div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
