<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <base href="" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="Connor, Connor Group, Asian Buying Agent, Sourcing Services, Global Sourcing, Supply Chain Management, Social Compliance, Technical Evaluation, Quality Management, Order Tracking, Furniture, ISO9002, World Class Service, Client Centric." />
  <meta name="title" content="What We Are" />
  <meta name="author" content="Basil Wong" />
  <meta name="description" content="The Connor Group is a world-wide merchandise-sourcing organization, managing the global sourcing requirements of over 60 companies." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Home | International Company in merchandise sourcing</title>
  <link href="https://www.weconnor.com/templates/Weconnor_Inner/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/css/style.css" type="text/css" />
  <script type="text/javascript" src="media/system/js/mootools.js"></script>
  <script type="text/javascript" src="media/system/js/caption.js"></script>
  <script type="text/javascript" src="media/system/js/swf.js"></script>
  <script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
  <script type="text/javascript" src="/js/jquery.innerfade.js"></script>

<!-- <link type="text/css" rel="stylesheet" href="/css/style.css" /> -->

<link type="text/css" rel="stylesheet" href="/css/layout.css" />
<link type="text/css" rel="stylesheet" href="/css/menu.css" />
<link type="text/css" rel="stylesheet" href="/css/joomla.css" />
<link type="text/css" rel="stylesheet" href="/css/template.css" />
<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie8.css" />
<![endif]-->
<!--[if IE 7]>
<link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie7.css" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="/js/jquery.splashdiv.js"></script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify.2.1.0.js"></script>
<script type="text/javascript" src="/js/infinityforms.js"></script>

<script type="text/javascript">
    var jQuery14 = jQuery.noConflict();
    
    

    // Custom javascripts
    //$("#inf_menu_main li.lv_1.item_33 a span").css("border","13px solid red");
    jQuery14(document).ready(function() {
    //    jQuery14(".mainmenu #inf_menu_main li.lv_1.item_33 a").hover(function(){
    //        jQuery("#div_hidden_1").show();
    //    }, function(){
    //        jQuery("#div_hidden_1").hide();
    //    });
      // alert("ok");   

    
//      jQuery14("#dlg_splash").splashDiv({
//            hoverHold:true
//          , cookieCheck:true
//          , height: 520
//          , show: {effect: 'blind', duration: 1000}
//          , hide: {effect: 'blind', duration: 500}
//          , onShow: function() {
//              jQuery14("#dlg_splash_btn_close").unbind('click');
//              jQuery14("#dlg_splash_btn_close").bind('click',function(){
//                  jQuery14("#dlg_splash").dialog("close");
//              });
//            }
//          //, onHide: function() {alert('hide');}
//      });
       
     

    });

    function hideContent(d) {
        document.getElementById(d).style.display = "none";
    }

    function showContent(d) {
        document.getElementById(d).style.display = "block";
    }

</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23262756-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>
</head>
<body id="home">
<?php
include("header.php");
?>
  
<div id="maincontainer">
<div class="main">
<!-- banner -->
<div class="banner">
<div class="moduletable">

    </div>
  
            </div>
            <!-- banner -->
            <!-- Breadcrumbs -->
            <div class="Mod_Breadcrumbs">
                
            </div>
            <!-- Breadcrumbs -->
            <div class="container">
              
    <div class="row">   
<div id="page">
<div class="col-md-10" style="margin-left: 30px;">

<h1 class="contentheading">
 Contact Us</h1>
<p>For more information, please e-mail: info@wesource.biz Or fill in the contact us form here.</p>
<p>We have formed a partnership with EthicsPoint  to provide a comprehensive and confidential reporting tool. This reporting system allows people to report issues or concerns they may have in an anonymous and confidential manner.</p>
</p><br/>
<div class="row">
<div class="col-md-8">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3498.3574219476805!2d77.13103431463243!3d28.738743686017667!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d0110e3aaeae5%3A0x5a3d03d4e45aff1!2sMakeUBig!5e0!3m2!1sen!2sin!4v1529650272944" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<div class="col-md-4">
<form name="contact-form" method="POST" action="form.php">
    <?php 
    $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    ?><h1 class="contentheading">
 Contact Us</h1>
<fieldset class="form-group">
  <label for="name">Your Name (required)</label>
  <input type="text" class="form-control" name="name" placeholder="Enter Your Name" required>
</fieldset>

<fieldset class="form-group">
  <label for="email">Email Id (required)</label>
  <input type="email" class="form-control" name="email" placeholder="Enter Your Email" required>
</fieldset>

<fieldset class="form-group">
  <label for="phone">Mobile Number (required)</label>
  <input type="text" class="form-control" name="phone" placeholder="Enter Your Phone" required="">
</fieldset>

<fieldset class="form-group">
  <label for="message">Your message (required)</label>
  <textarea name="message" class="form-control" rows="2" required></textarea>
</fieldset>
<input type="hidden" name="redirect" value="<?= $link;?>" />
<button type="submit" class="btn btn-success" required>SEND MESSAGE</button>
</form>
</div>
<br/><br/><br/>
<div class="" style="margin-left: 1em;">
<h1 class="contentheading">Corporate Office </h1>
<p>K502 Plumeria Garden Estate<br>
Sector-Omnicron 3 <br>
Greater Noida 201308,
Uttar Pradesh.<br>
<b>Email- info@wesource.biz<br>
Tel- 91 120 4120110<br>
</p>
</div>
</div></div>

<div class="clear-both"></div>
</div>

</div>
</div>  

</div>
    
<?php include("footer.php"); ?>
</div>
</div>
</div>



</body>
</html>