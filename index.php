<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <base href="" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="Connor, Connor Group, Asian Buying Agent, Sourcing Services, Global Sourcing, Supply Chain Management, Social Compliance, Technical Evaluation, Quality Management, Order Tracking, Furniture, ISO9002, World Class Service, Client Centric." />
  <meta name="title" content="What We Are" />
  <meta name="author" content="Basil Wong" />
  <meta name="description" content="The Connor Group is a world-wide merchandise-sourcing organization, managing the global sourcing requirements of over 60 companies." />
  <meta name="generator" content="Joomla! 1.5 - Open Source Content Management" />
  <title>Home | International Company in merchandise sourcing</title>
  <link href="https://www.weconnor.com/templates/Weconnor_Inner/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="/css/style.css" type="text/css" />
  <script type="text/javascript" src="media/system/js/mootools.js"></script>
  <script type="text/javascript" src="media/system/js/caption.js"></script>
  <script type="text/javascript" src="media/system/js/swf.js"></script>
  <script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
  <script type="text/javascript" src="/js/jquery.innerfade.js"></script>

<!-- <link type="text/css" rel="stylesheet" href="/css/style.css" /> -->

<link type="text/css" rel="stylesheet" href="/css/layout.css" />
<link type="text/css" rel="stylesheet" href="/css/menu.css" />
<link type="text/css" rel="stylesheet" href="/css/joomla.css" />
<link type="text/css" rel="stylesheet" href="/css/template.css" />
<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css" />
<!--[if IE 8]>
<link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie8.css" />
<![endif]-->
<!--[if IE 7]>
<link type="text/css" rel="stylesheet" href="/templates/Weconnor_Home/css/ie7.css" />
<![endif]-->
<link type="text/css" rel="stylesheet" href="/css/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="/js/jquery.splashdiv.js"></script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<script type="text/javascript" src="/js/uploadify.2.1.0.js"></script>
<script type="text/javascript" src="/js/infinityforms.js"></script>

<script type="text/javascript">
    var jQuery14 = jQuery.noConflict();
    
    

    // Custom javascripts
    //$("#inf_menu_main li.lv_1.item_33 a span").css("border","13px solid red");
    jQuery14(document).ready(function() {
    //    jQuery14(".mainmenu #inf_menu_main li.lv_1.item_33 a").hover(function(){
    //        jQuery("#div_hidden_1").show();
    //    }, function(){
    //        jQuery("#div_hidden_1").hide();
    //    });
      // alert("ok");   

    
//      jQuery14("#dlg_splash").splashDiv({
//            hoverHold:true
//          , cookieCheck:true
//          , height: 520
//          , show: {effect: 'blind', duration: 1000}
//          , hide: {effect: 'blind', duration: 500}
//          , onShow: function() {
//              jQuery14("#dlg_splash_btn_close").unbind('click');
//              jQuery14("#dlg_splash_btn_close").bind('click',function(){
//                  jQuery14("#dlg_splash").dialog("close");
//              });
//            }
//          //, onHide: function() {alert('hide');}
//      });
       
     

    });

    function hideContent(d) {
        document.getElementById(d).style.display = "none";
    }

    function showContent(d) {
        document.getElementById(d).style.display = "block";
    }

</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-23262756-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>
</head>
<body id="home">
<?php
include("header.php");
?>
	
<div id="maincontainer">
<div class="main">
<!-- banner -->
<div class="banner">
<div class="moduletable">


<div class="infbanners" id="infbanners-1">
	<div class="banners" id="infbanners-1-frame" style="height: 300px!important;">
		                    <div class="banner" id="tab1">
                                            <a href="home#">
                        <img style="width: 1040px;" alt="Desert" src="/images/1.jpg" title="slideshow5">
                    </a>
                </div>
		                    <div class="banner" id="tab2">
                                            <a href="home#">
                        <img style="width: 1040px;" alt="Desert" src="/images/2.jpg" title="slideshow1">
                    </a>
                </div>
		                    <div class="banner" id="tab3">
                                            <a href="home#">
                        <img style="width: 1040px;" alt="Desert" src="/images/3.jpg" title="slideshow2">
                    </a>
                </div>
		                    <div class="banner" id="tab4">
                                            <a href="home#">
                        <img style="width: 1040px;" alt="Desert" src="/images/4.jpg" title="slideshow3">
                    </a>
                </div>
		                    <div class="banner" id="tab5">
                                            <a href="home#">
                        <img style="width: 1040px;" alt="Desert" src="/images/5.jpg" title="slideshow4">
                    </a>
                </div>
			</div>
		<div class="balls" >
            <ul>
                            <li class="tab1"><a title="slideshow5" href="javascript:void(null);"></a></li>
                            <li class="tab2"><a title="slideshow1" href="javascript:void(null);"></a></li>
                            <li class="tab3"><a title="slideshow2" href="javascript:void(null);"></a></li>
                            <li class="tab4"><a title="slideshow3" href="javascript:void(null);"></a></li>
                            <li class="tab5"><a title="slideshow4" href="javascript:void(null);"></a></li>
                        </ul>
            <div class="clear-both"></div>
	</div>
		</div>


<script type="text/javascript">
jQuery.noConflict();
(function(jq) {
    jq('#infbanners-1-frame').innerfade({
            speed: 'slow',
            animationtype: 'fade',
            timeout: 8000,
            containerheight: '220px',
            bar: '#infbanners-1 .balls'
    });

})(jQuery);

</script>

		</div>
	
            </div>
            <!-- banner -->
            <!-- Breadcrumbs -->
            <div class="Mod_Breadcrumbs">
                
            </div>
            <!-- Breadcrumbs -->
            <div class="container">
            	
		<div class="row">		
<div id="page">
<div class="col-md-10" style="margin-left: 30px;">

<h1 class="contentheading">
	What We Are</h1>

<p><strong>WESOURCE</strong> is sourcing services start up located at the heart of India. WESOURCE founded by group of professionals worked in technology, sourcing and quality assurance. </p>
<p>We represent our overseas and domestic clients such as retailers, importers, wholesalers speciality stores and not only source furniture, decorative accessories, wall décor, textile on their behalf but also looks after PO management, product management, and quality assurance. 
</p>

</div>
</div></div>

<div class="clear-both"></div>
</div>

</div>
</div>	

</div>
    
<?php include("footer.php"); ?>
</div>
</div>
</div>



</body>
</html>